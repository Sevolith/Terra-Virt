resource "libvirt_volume" "local-qcow2" {
  name   = "local-qcow2"
  source = "/var/lib/libvirt/images/deb10.qcow2"
  pool   = "default"
  format = "qcow2"
}